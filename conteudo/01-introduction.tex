%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Introduction}
\label{chap:introduction}

Device drivers are an important part of the Linux kernel and represent about
66\%\footnote{Estimate calculated with data from cloc tool:
\url{https://github.com/AlDanial/cloc}} of the project code lines. Although the
code portion that drivers represent in a conventional operating system (OS) can
vary, some of these components are indispensable to the system's operation.  In
addition, the Linux kernel is widely used in a series of applications as cloud
service providers, embedded systems, smartphones, and
supercomputers~\citep{lf:kernelreport17}. Thus, testing is fundamental to
increase the Linux kernel's confidence level and the operation of GNU/Linux
systems submitted to different workloads. In particular, device drivers require
differentiated approaches because they are relatively difficult to test. A fact
that instigates to question: how are device drivers being tested? This work
tries to find out how developers are testing Linux kernel device drivers.

%Obs.: cloc foi usado no 2020 Linux Kernel History Report da LF

\section{Problem outline}
\label{sec:problem-outline}

From the experience of our research group in Linux kernel development and a
systematic mapping study conducted during this work, we have found evidence
that (1) the testing tools proposed by academic works are not being used by the
Linux development community and that (2) the testing tools that kernel
developers are using are not well covered in the academic literature.

There is a disconnection between academia and industry regarding practices for
testing Linux kernel device drivers. On the one hand, Kernel
developers do not adopt the testing tools proposed by academia. On the other
hand, test tools extensively promoted by the Linux community are not the
object of academic study. This scenario leads us to consider that academia does
not fully understand the testing needs of Linux kernel developers, whereas the
project may not leverage innovative ideas published by academia.

Thus, we identified the following research problems:

\begin{itemize}
\item There are no references in the academic literature reporting the testing
practices and tools used by the Linux kernel development community.
\item Researchers spend time developing several software testing tools, but the
Linux kernel development community does not adopt them.
\item No publication approaches Linux kernel device driver developer's
difficulties regarding driver testing.
\end{itemize}

\section{Research Objectives}

By including the Linux kernel as our research target, we cannot ignore the role
of the community in maintaining such a massive codebase. In recent years, more
than 4000 people have contributed to the advancement of the Linux kernel each
year~\citep{lf:kernelreport2020}.  Thus, we outline our goals to contribute to
both academia and the development community.

We pursued the following research objectives:

\par \emph{RO1.} Provide a list of tools used by the Linux kernel community
to test the Linux kernel.

\par \emph{RO2.} Identify testing strategies and tools used by Linux kernel
device driver maintainers.

\par \emph{RO3.} Improve coverage of formal literature regarding the
state-of-the-practice of Linux kernel device driver testing.

By achieving \emph{RQ1}, we produced a list of test tools that provide a
reference for further academic works related to software testing and the Linux
kernel. In accomplishing \emph{RO2}, we boost researchers and practitioners by
pointing out promising paths for those considering studying or contributing to
existing tools. Upon reaching \emph{RO3}, we hope researchers can make
better-guided decisions when proposing new testing tools for the Linux kernel or
even consider contributing to existing ones. We have evidence that some of the
test tools introduced by academics have been discontinued and fallen into
disuse. Falling into disuse, however, is not a phenomenon restricted to Linux
kernel testing tools. About 40\% of the static analysis tools published in ASE
(International Conference on Automated Software Engineering) and SCAM
(International Working Conference on Source Code Analysis \& Manipulation)
between 1991 and 2015 are in a closedown stage~\citep{Joenio2018}.

\section{Research Questions and Plan}

The problems described in Section~\ref{sec:problem-outline} suggest an update of
the academic literature. Furthermore, our objectives require the observation of
subjective aspects linked to the community. After all, the perception of a
contribution as an improvement depends on the community's opinion. Linux kernel
maintainers accept patches only when submitters convince them the proposed
changes are beneficial. Patches are pulled to the kernel, not pushed. While it
is possible to ask many questions about the testing practices adopted by the
Linux community, we limited the scope of this work to the procedures focused on
testing device drivers. Assessing test methods from all the Linux subsystems
would require investigating a much larger material. In this work, we seek to
provide reasonable answers to the following questions:

\par
\emph{RQ1.} How are Linux device drivers being tested?

\par
\emph{RQ2.} What testing tools are being used by the Linux community to test
the kernel? What are the main features of these tools?

\par
\emph{RQ3.} What features does the community desire for a testing tool?
About the tools that are already in use, what could be improved?

This research took place in 3 primary phases. In the first phase, we conducted a
mapping study; in the second phase, we conducted a grey literature review (GLR);
and in the third phase, we conducted a survey with Linux kernel device driver
maintainers. We then triangulate our findings from academic papers, informal
literature, and community developers by synthesizing different points of view on
device driver testing.

\section{Thesis Structure}

The remaining of this work is structured in four more chapters. Chapter 2
provides some background on subjects related to the Linux kernel device driver
and software testing. We describe the applied research methods in Chapter 3.
Chapter 4 presents and examines the results of our research. In Chapter 5, we
conclude this work by discussing the limitations of our work and possible future
investigations.

%Figure~\ref{fig:research-design} shows the workflow of the main activities
%conducted and planned for this research.

%\begin{figure}
%  \centering
%  \includegraphics[width=1.0\textwidth]{caminho-pesquisa}
%  \caption{Research workflow design.\label{fig:research-design}}
%\end{figure}
