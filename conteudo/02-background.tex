%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Background}
\label{chap:background}

To discuss the state-of-the-practice of Linux kernel
testing, it is worth briefly reviewing some related concepts.

\section{Linux kernel device drivers}

The Linux kernel is accounted for many essential operating system tasks such as
memory management, process scheduling, data storage, network communication, and
many others~\citep{kernel:about}. The kernel must operate several devices with
highly distinct characteristics and complexity to provide fundamental system
functionality. Moreover, it is reasonable to avoid mixing the control logic of
different devices with one another and with core system logic. Thus, it is usual
to encapsulate code for managing a device (or a family of related devices) into
a device driver. ``A driver is a piece of software whose aim is to control and
manage a particular hardware device, hence the name device
driver''~\citep{LDDD}. To be more specific, we consider that a device driver is
characterized by a well-delimited piece of code (usually a file or a few files)
whose purpose is to control the operation of a hardware design (or a set of
related hardware designs). Usually, hardware designs are described in documents
called datasheets or blueprints, which in turn describe the components and
operation of a hardware device.

Entire subsystems and kernel portions not restricted to the operation of a
single device (or set of devices), e.g., file systems, network stack, process
scheduler, memory manager, etc., are not device drivers. Although these
components may contain device drivers, they are not device drivers. Next, we
must attend to the concepts of software testing.

\section{Linux Kernel Development Process}

At each release, Linux incorporates changes from hundreds of developers
worldwide. These developers may contribute to the project in many ways, such as
by improving the documentation, fixing bugs, introducing new features, and
providing support for new device drivers. The contributions to Linux (often
called \textbf{patches}) are sent through email to appropriate mailing lists.
There are several mailing lists at which Linux developers and maintainers review
and discuss changes to the kernel.

When a maintainer accepts a patch, they include it in their development
repository. Many (if not all) Linux maintainers have their development
repositories (or \textbf{trees}) hosted at
Kernel.org\footnote{\url{https://git.kernel.org/}}. Since development trees are
publicly accessible, test rings may perform tests on early phases of Linux
kernel development. Moreover, Linux kernel developers and maintainers may
request test ring administrators to add their repositories to the test
infrastructure. After a repository is added to a test ring, it gets periodically
pulled for testing~\citep{lf:mentorship-kselftest,lf:mentorship-process}. Thus,
after a patch gets into a development tree, it may be subjected to many tests
from Linux kernel test systems.

The Linux kernel source code is logically divided into several subsystems. ``A
subsystem is a representation for a high-level portion of the kernel as a
whole~\citep{LDD3-subsys-def:OReilly}.'' A subsystem may also be understood as
an abstraction to refer to some part of the kernel responsible for some system
functionality, such as process scheduling, memory management, networking, etc.
Most subsystems have one or more developers who take the overall responsibility
for the code on that subsystem. These developers are known as subsystem
maintainers~\citep{kdoc:dev-process}.

At the beginning of each development cycle, Linus Torvalds declares he will
accept new features for the Linux kernel throughout a period known as the
\textbf{merge window}. Then, during a typically two-week span, subsystem
maintainers ask Linus to add (pull) changes from their trees into his repository
(known as the \textbf{mainline} kernel). After pulling patches during those
couple of weeks, Linus declares the merge window closed and stops merging new
features for the next Linux release. The kernel produced by Linus at the end of
a merge window is an artifact that urges testing since it is the bedrock for the
upcoming Linux release.

The weeks that follow the merge window are known as a stabilization period
during which Linus and many other kernel developers try to fix as many bugs and
regressions as possible. That is as also a time of intense testing by robots,
automated test systems, and test rings. The testing and bug-hunting season
usually lasts six to eight weeks until Linus declares the release candidate to
be the new Linux kernel release~\citep{kdoc:dev-process}. After that, Linus
opens a new merge window and the process repeats for a newer Linux release.

\subsection{Next trees}

Reviewers, testers, maintainers, and developers alike may want to view the
changes queued for the next kernel release in an integrated form so they can
avoid merging conflicts. However, pulling and merging patches from several
subsystem trees is a cumbersome and error-prone task.

To overcome this problem, the community provides \textbf{-next trees}, which
bring together patches from several subsystems. The main tree for merging
patches queued for the next Linux release is linux-next. By design, linux-next
is a snapshot of how the mainline should be after the next merge window
closes~\citep{kdoc:dev-process}. Since linux-next comprises changes intended for
the forthcoming Linux kernel, the tree may enable contributions to be tested
weeks ahead they reach the mainline. Refer to \citet{kdoc:dev-process} for a
detailed reference on the Linux kernel development process.

\section{Software Testing}

Software
testing is ``an activity in which a system or component is executed under
specified conditions, the results are observed or recorded, and an evaluation is
made of some aspect of the system or component''~\citep{ieee:glossary}.  We,
however, consider a slightly broadened concept of software testing by also
comprising compilation time code inspection such as checks made by static
analysis tools. We believe this notion better aligns with complementary
definitions of software testing. For instance, \citet{Claudi2011} stated that
``in software engineering, testing is the process of validation, verification
and reliability measurement that ensures the software to work as expected and to
meet requirements''. Likewise, \citet{Mathur:fst} suggests that software testing
``is the process of determining if a program behaves as expected.'' We
understand that analyzing the structures and properties of the source code may
provide insight into whether a program will function as desired, thus
possibly making part of a software testing activity.

Nevertheless, regardless of the program under test, it is intrinsic to the
software testing activity to compare measured behavior with the desired behavior
described by formal or informal requirements~\citep{Mathur:fst}. Moreover, many
hardware manufacturers provide datasheets describing the components and the
functioning of the devices they supply. Therefore, we may say that device driver
testing is the act of evaluating, validating, or verifying whether a device
driver (or parts of it) operates the hardware it supports as described by its
design. However, the reality is more complicated than the theory, meaning not
all hardware devices work precisely as their datasheets describe or even come
with accessible blueprints. In such cases, device drivers must also deal with
nonconforming behavior or resort to reverse engineering.

\citet{Mathur:fst} proposed a comprehensive categorization of software testing
techniques based on four classifiers. The classifiers ponder the resources for
generating the tests, the development phase in which the tests are carried out,
the objective of the test activity, and the characteristics of the artifact
under test. However, those classifiers are not well suited for distinguishing
between Linux kernel test practices for a few reasons.

First, Linux is a free software project, so the source code is accessible to
everyone. Therefore, the kernel source could be used as an inspiration for any
testing practice. Thus, testing techniques usually classified as black-box could
be considered black-box and white-box. For instance, Andrey Konovalov advises
reading the code to understand what types of input the system expects and to
identify which parts of it can be targeted in the context of fuzzing the Linux
kernel~\citep{lf:fuzzing}.

In addition, a kernel developer has access to all phases of the project
lifecycle. They can run unit tests on the initial versions of the developed
code. There is even a framework called KUnit for writing unit tests built into
the kernel. After making the desired changes, developers may compile and install
the kernel for integration testing. At this testing phase, subsystem code can be
exercised manually or with the help of additional testing tools such as
Kselftest. Also, kernel developers often work on improvements to existing
functionality. In such cases, the tests performed can be considered regression
tests. Since GNU/Linux distributions use or adapt the mainline or stable kernels
to make the operating system, testing against any of these trees is also a form
of beta-testing~\citep{fedora:linux, fedora:kernel, debian:about}.

Also, developers are interested in testing the Linux kernel to identify
regressions. When responding to a bugfix rollback, \citet{lwn:revert-fix} says
why regressions are particularly unwanted in the kernel:

\begin{displayquote}
Because it is much more important to make slow, but steady progress and have
people know things improve (or at least not ``deprove''). We do not want any
kind of ``brownian motion development''.
\end{displayquote}

Other reasons to test the kernel may include checking the behavior under
invalid inputs or high workloads, verifying compatibility with external
components, investigating security aspects, and more. Thus, robustness testing,
stress testing, interface testing, and security testing are examples of tests
to which the Linux kernel can be submitted. There are no GUI tests as Linux
does not contain any GUI components.

Finally, considering the artifact under test classifier, testing techniques
applied over Linux can be classified as operating system testing or merely code
testing. That said, much of the testing over the Linux kernel would be
classified as black-and-white-box regression OS testing according to Mathur's
classifiers. However, to provide a more informative categorization of kernel
testing tools, we renounce Mathur's classifiers to appraise the means used to
perform kernel tests. Thus, we decided to consider as a test technique the
answers to the question: what was done to test X? Where X is some device driver
or parts of one. For instance, someone could decide to test a device driver by
feeding random values to its interface (fuzzing), creating a model of it and
using properties of that model to perform tests (model-based testing),
instrumenting the code to measure runtime activity (performance testing, stress
testing), etc.

Furthermore, different test techniques imply different stages of the generation
and execution of test cases and subsequent analysis of results. For example,
tools based on models (model-based testing) start from a program model for test
generation. Fault injection tools need to instrument code or intercept system
calls to test target software. In both cases, an essential preparation step is
done either before specifying which properties to test or before defining fault
injection sites. We decided to look upon such preliminary activities as part of
the test case generation. We have regarded the tasks related to the execution of
software components and the control of the conditions under which code execution
takes place solely as ``test execution''. In addition, we took the term ``test
assessment'' to refer to the activities related to assessing aspects of the
software under test. With this, it was possible to display the characteristics
of the tools concisely in Table~\ref{tab:papers} (presented in Chapter 4).

%Ainda na Table~\ref{tab:papers} eu simplifiquei a coluna "Automated" para
%mostar as informações de forma concisa.

%resumi as atividades relacionadas a testes em três etapas: geração de casos de
%teste, execução, e avaliação.

%para tornar a visualização das etapas de teste automatizadas mais concisa
%na Table~\ref{tab:papers}.


%Ainda com o objetivo de apresentar as informações de forma sintética,
%consideramos todas as atividades relacionadas à execução de um ou mais
%componentes de software e o controle das condições em que sua execução ocorre
%simplesmente como "test execution".
