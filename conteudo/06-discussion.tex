%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Triangulation and Discussion}
\label{chap:discussion}

Now that we have collected information from three different research methods
(formal literature, grey literature, and community survey), let's compare our
findings and merge them through synthesis by integration. According to
\citet{denise2008}, synthesis by integration is characterized by comparing
evidence involving two or more data collection methods. Likewise, we
triangulated the data from our research to corroborate a comprehensive view of
how Linux kernel device drivers are being tested (\emph{RQ1}).

\section{Triangulation}

Overall, we did not identify any conflicting information between formal and grey
literature publications. However, we note that grey literature publications did
not mention any test tool introduced by the academic papers we assessed in this
study. Therefore, we focus on comparing information about tools reported in
community publications.

Starting with LTP, \citet{lj:lk-testing} and \citet{ltp:howto} say that LTP
contains a collection of tools to test the reliability, robustness, and
stability of the Linux kernel and related features. Further,
\citet{Zaidenberg2015} adds that the test suite methodology is also based on
regression testing.

Still talking about the Linux Test Project, \citet{Claudi2011} states that LTP
can test essential Linux kernel features such as filesystems, device drivers,
memory management, scheduler, disk I/O, networking, syscalls, and IPC.
Similarly, \citet{lj:lk-testing} says that, by default, LTP run script includes
tests for filesystems, disk I/O, memory management, inter process communication
(IPC), the process scheduler, and the system call interface. Also, both
\citet{Claudi2011} and \citet{lj:lk-testing} assert that some Linux testing
projects are built on top of LTP or incorporate it somewhat. Regarding device
driver testing, \citet{Renzelmann2012} reported that LTP can invoke drivers and
verify their behavior, but it requires the device to be present. They also
mention that LTP cannot verify properties of individual driver entry points
because it runs tests at the system-call level.

\citet{Rothberg2016} were consistent with Coccinelle's home page
\citep{coccinelle:home} when they cited Coccinelle as a Linux kernel static
analysis tool that also helps code collateral evolution. They've also
acknowledged the activity of continuous integration (CI) testing services,
rating the 0-day test robot as the most prominent tool among automated testing
services. One year later, \citet{lf2017:report} ranked 0-day as the top bug
reporter during the development period between 4.8 and 4.13 Linux kernel
releases.

\citet{Chen2013} introduced a test tool called KIS (Kernel Instant bug testing
Service) which runs static analysis tests with GCC, Sparse, Smatch, and
Coccinelle. They also mentioned that KIS runs dynamic analysis tests with
Trinity, xfstests, and mmtests. In turn, \citet{Chen2020} commented that dynamic
analysis tools have been receiving more attention in recent years and mentioned
Syzkaller among fuzz testing tools.

With respect to Kselftest, both \citet{kdoc:kselftest}, \citet{selftest:start},
and \citet{Rothberg2016} say the test suite has unit tests that should execute
quickly and exercise individual code paths.

Thus, formal and grey literature complement each other in describing Linux
kernel test tools.

\section{Discussion}

We highlight that neither the community publications reviewed by us nor the
responses to survey questions 8 and 12 have mentioned any testing tool
introduced by academic papers assessed during our mapping study. In addition,
the results from survey question 11 indicate that no device driver maintainer
uses any of the testing tools proposed by academic papers we've assessed.
Therefore, we consider that the Linux community is not using those test tools
presented by academic studies to test the kernel.

Instead, we back on three pieces of evidence to provide a list of the test tools
used by the Linux community to test the kernel (\emph{RQ2}). The first piece of
evidence we consider is the collection of responses to survey question 8 (``Do
you use any tool to test the Linux kernel yourself? If so, which?'') and to
question 12 (``Should we consider any other tool for device driver testing? If
so, which?''). These were open-ended (non-suggestive) questions in which device
driver maintainers could report any tool they felt they used for testing the
kernel. We will denote evidence from answers to those questions by the
\textit{spontaneous} keyword.

The second piece of evidence we consider comes from the collection of test tools we
organized from information gathered during our mapping study and grey literature
review (Table~\ref{tab:lit-tools}). Every tool there was referenced by at least
one publication, even though many were mentioned by three articles or more.
Moreover, we have inspected the repositories of each tool in that table and
estimated their activity status based on whether they have received
contributions from January 2021 to March 2022. Thus, we consider (and denote)
that any tool in Table~\ref{tab:lit-tools} regarded as being actively developed
has \textit{activity} evidence in favor of it.

%If a particular tool got at least one reply asserting a driver maintainer uses
%it at least ocassionally, then we recognize that as an \textit{usage frequency}
%evidence indicating that specific tool being used.

The third and last piece of evidence we considered is the set of responses to
survey question 11 (``How familiar are you with these tools/testing
infrastructure?''). This question presented a list of twenty tools we assessed
throughout our research and offered five options to let respondents report if
they knew about those tools and how often they used each of them. If a
particular tool got at least one reply asserting a driver maintainer uses it,
then we understand such response as \textit{usage frequency} evidence indicating
that the specific tool is being used.

%sim g3q4, sim tab, sim g4q3
With that, we estimate with high confidence that Coccinelle, KernelCI,
Kselftest, ktest, KUnit, Smatch, Sparse, and Syzkaller/Syzbot are being used by
the community to test Linux. These tools have \textit{spontaneous},
\textit{activity}, and \textit{usage frequency} pieces of evidence indicating
their use.

%use g4q3, não g3q4, sim tab
In addition to those, we also have high confidence that kernel developers use
0-day, LKFT, LTP, Trinity, and TuxMake because these are supported by
\textit{activity} and \textit{usage frequency} pieces of evidence.

%sim g3q4, sim tab, não g4q3
Another few tools we highly believe are being used by Linux developers are perf,
igt-gpu-tools, and kvm unit tests. \textit{Spontaneous} and \textit{activity}
pieces of evidence support these.

%sim g3q4, não tab, não g4q3, sim g4q4
Some other tools reported in responses to question 8 or question 12 were not
present in Table~\ref{tab:lit-tools} either because we did not identify them
during our literature review or because we disregarded them as test tools.
Consequently, we did not assess their development activity status or usage nor
presented them in question 11. Anyway, we list those tools here since they have
\textit{spontaneous} evidence of use and thus are probably being used by device
driver maintainers, even though we are not sure all of them fit the software
test category. These tools are kernel\_patch\_verify, rdma tools, tcpdump,
lockdep, piglit, slub, pps-tools, roadtest, tools/testing/cxl, virtme, coverity,
cec-compliance, checkpatch, can-utils, mdio-tools, ethtool, ping,
kmemleak, qemu, kasan, kcsan, klocwork.

%active tab, não g3q4, não g4q3
Lastly, Table~\ref{tab:lit-tools} has tools that were not mentioned in responses to
questions 8, 11, or 12. Nevertheless, we believe that Linux developers may use
those tools which only have \textit{activity} pieces of evidence in their favor.
However, we cannot estimate a usage probability for those tools because they
differ in the number of publications mentioning each one. Also, we perceive
different data sources with slightly different regards. Thus, the test tools
with only \textit{activity} evidence supporting them may have a low to high
chance of being used by the Linux community. To simplify the visualization, we
added the number of referencing publications after each activity evidence marker
in Table~\ref{tab:rq2-tools}. Table~\ref{tab:rq2-tools} shows the complete list
of testing tools being used by the Linux community to test the kernel
(\emph{RQ2}, \emph{RO1}) and the types of evidence supporting them.

\begin{longtable}[c]{|l|c|}
% O cabeçalho da tabela na primeira página em que ela aparece.
\hline
\captionlistentry{Test tools used by the Linux community to test the Linux kernel.}
\label{tab:rq2-tools}
\emph{Tool}& \emph{Supporting evidence} \\
\hline \hline
\endfirsthead % Final do cabeçalho que aparece na primeira página
% O cabeçalho da tabela em todas as páginas em que ela aparece
% exceto a primeira; aqui, igual ao anterior
\hline
\emph{Tool}& \emph{Supporting evidence} \\
\hline \hline
\endhead % Final do cabeçalho das páginas seguintes à primeira
% O rodapé da tabela em todas as páginas em que ela aparece
% exceto a última
\hline
\multicolumn{2}{|r|}{\textit{continue}\enspace$\longrightarrow$}\\
\hline
% Como usamos \captionlistentry mais acima, usamos "[]" aqui.
\caption[]{Test tools used by the Linux community to test the Linux kernel.}
\endfoot % Final do rodapé que aparece em todas as páginas exceto a última
% O rodapé da tabela na última página em que ela aparece
\hline
% Como usamos \captionlistentry mais acima, usamos "[]" aqui.
\caption[]{Test tools used by the Linux community to test the Linux kernel.}
\endlastfoot % Final do rodapé da última página
% O conteúdo da tabela de fato.
\hline
%\midrule
Coccinelle    & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
KernelCI      & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
Kselftest     & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
ktest         & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
KUnit         & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
Smatch        & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
Sparse        & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
Syzkaller/Syzbot & \textit{spontaneous}, \textit{activity}, \textit{usage frequency} \\
\hline
0-day      & \textit{activity}, \textit{usage frequency} \\
\hline
LKFT       & \textit{activity}, \textit{usage frequency} \\
\hline
LTP        & \textit{activity}, \textit{usage frequency} \\
\hline
Trinity    & \textit{activity}, \textit{usage frequency} \\
\hline
TuxMake    & \textit{activity}, \textit{usage frequency} \\
\hline
perf            & \textit{spontaneous}, \textit{activity} \\
\hline
IGT GPU Tools   & \textit{spontaneous}, \textit{activity} \\
\hline
KVM Unit Tests  & \textit{spontaneous}, \textit{activity} \\
\hline
kernel\_patch\_verify     & \textit{spontaneous} \\
\hline
RDMA Tools                & \textit{spontaneous} \\
\hline
tcpdump                   & \textit{spontaneous} \\
\hline
lockdep                   & \textit{spontaneous} \\
\hline
Piglit                    & \textit{spontaneous} \\
\hline
SLUB                      & \textit{spontaneous} \\
\hline
pps-tools                 & \textit{spontaneous} \\
\hline
roadtest                  & \textit{spontaneous} \\
\hline
CXL Tests                 & \textit{spontaneous} \\
\hline
virtme                    & \textit{spontaneous} \\
\hline
Coverity                  & \textit{spontaneous} \\
\hline
cec-compliance            & \textit{spontaneous} \\
\hline
checkpatch                & \textit{spontaneous} \\
\hline
can-utils                 & \textit{spontaneous} \\
\hline
mdio-tools                & \textit{spontaneous} \\
\hline
ethtool                   & \textit{spontaneous} \\
\hline
ping                      & \textit{spontaneous} \\
\hline
kmemleak                  & \textit{spontaneous} \\
\hline
QEMU                      & \textit{spontaneous} \\
\hline
KASAN                     & \textit{spontaneous} \\
\hline
KCSAN                     & \textit{spontaneous} \\
\hline
klocwork                  & \textit{spontaneous} \\
\hline
Dr. Checker & \textit{activity} (1) \\
\hline
DIFUZE & \textit{activity} (1) \\
\hline
mmtest & \textit{activity} (1) \\
\hline
KCOV & \textit{activity} (1) \\
\hline
cyclictest & \textit{activity} (1) \\
\hline
hackbench & \textit{activity} (3) \\
\hline
rcutorture & \textit{activity} (2) \\
\hline
Hulk Robot & \textit{activity} (2) \\
\hline
Buildbot & \textit{activity} (1) \\
\hline
Continuous Kernel Integration (CKI) & \textit{activity} (2) \\
\hline
AutoTest & \textit{activity} (2) \\
\hline
xfstests & \textit{activity} (3) \\
\hline
LAVA - Linaro Automated Validation Architecture & \textit{activity} (2) \\
\hline
Fuego & \textit{activity} (2) \\
\hline
KMSAN & \textit{activity} (1) \\
\hline
LISA & \textit{activity} (2) \\
\hline
KTSAN & \textit{activity} (1) \\
\hline
LKMM / litmus-test & \textit{activity} (1) \\
\hline
herd & \textit{activity} (2) \\
\hline
TuxSuite & \textit{activity} (1) \\
\hline
Schbench & \textit{activity} (1) \\
\hline
Rt-app & \textit{activity} (1) \\
\hline
Phoronix Test Suite & \textit{activity} (1) \\
\hline
Marvin & \textit{activity} (1) \\
\hline
Undertaker & \textit{activity} (1) \\
\hline
S Suite & \textit{activity} (1) \\
\end{longtable}

As for the features of these tools, some provide unit tests, and others do
integration tests, stress tests, fuzz testing, and many other types of tests
listed in the \textit{Testing Techniques} column of Table~\ref{tab:lit-tools}.
Most of the tools in Table~\ref{tab:rq2-tools} may be run by individual
developers, and some of them are run by automated test services that fetch and
test patches from mailing lists or from Linux kernel trees. Detailed information
about the tools that we assessed during this research and their capabilities
were presented in Subsection~\ref{subsec:glr-tools}.

Coming back to the broader research question \emph{RQ1}, we now use the evidence
collected throughout this study to outline how Linux device drivers are tested.
We will divide the answer to this question into two parts: tests performed by
individual community members who develop, maintain, or test Linux device
drivers; and tests performed by automated test systems and CI rings.

First, according to our survey results, 90\% of Linux kernel device driver
maintainers do test patches frequently (always or, at least, most times) before
sending them to any mailing list. When testing drivers they maintain, 84\% of
Linux developers do at least basic soundness tests such as probing and binding
drivers to devices they supports. Moreover, 68\% of driver maintainers also
assess driver functionality through kernel space to user space ABI, and 38\% of
them run test tools to catch potential bugs. Respectively, 19\% and 15\% of
driver maintainers who always runtime test their patches said to run either
static or dynamic analysis tools very often (always or, at least, most times).
With some regularity, 42\% of device driver maintainers run Sparse, 20\% run
Smatch, 16\% run Kselftest, 16\% run Coccinelle, 15\% run Syzkaller, 7\% run
KUnit, 7\% run LTP, 4\% run Trinity, 2\% run TuxMake, and 2\% of them run ktest.
%some regularity = (often or, at least, sometimes)

Throughout our GLR, we have found a few examples of developers and companies
said to have been testing the Linux kernel. The Linux Foundation carried on a
series of interviews with Linux kernel developers. To them, Arnd Bergmann said
to have started doing a lot of build-testing to improve the quality of merged
contributions~\citep{lf:arnd}. Laura Abbott said to have spent a lot of time
testing and reviewing patches for kernel hardening~\citep{lf:laura}. Shuah Khan
said to have boot tested stable kernel release candidates~\citep{lf:shuan}. A
kernel development report revealed that Linus Torvalds routinely boot tests the
kernel that results after accepting a pull request~\citep{lf2017:report}.
Another report from the Linux Foundation pointed out that the Real-Time Linux
development team will have to test and adjust new incoming features to maintain
the kernel's real-time capability~\citep{lf:rt}. Companies seem to have more
specific interests. Collabora intends to have the DRM subsystem under continuous
integration, validating new drivers with the IGT test suite~\citep{lc:kernelci}.
ARM and Linaro use a real-time workload simulator called rt-app to trigger
specific scheduler code paths to test small scheduling and load-balancing
changes~\citep{lwn:rt-app}. Oracle tests Linux kernels with workloads related to
their products, such as Oracle Engineered Systems, Oracle Cloud Infrastructure,
and enterprise deployments for Oracle customers~\citep{lf:oracleuek}.

Besides the tools mentioned above, evidence indicates that another myriad of
software is used to test Linux device drivers. Table~\ref{tab:rq2-tools} lists
the tools for which we have acquired evidence of usage in the context of Linux
kernel testing. Even though not all of those are strictly test tools, most of
them are available to anyone. Thus, many other individual developers, who are
not necessarily driver maintainers, may be running them to test Linux somehow.

Speaking about things that are not precisely test tools, many survey
participants have reported test practices not related to any test tool in
particular. The most mentioned of those practices was using hardware emulation,
device models, or mocks, to carry on the tests over device drivers.
Particularly, QEMU was mentioned by some maintainers as a tool that allowed such
tests. In addition, a few other maintainers said running custom scripts or using
in-kernel functionality (such as fault injection capabilities) to test device
drivers. We estimate that the number of developers performing tests through
custom scripts, QEMU models, or manual inspection is not negligible. 30\% of
driver maintainers who said to always runtime test their patches don't use any
static or dynamic analysis tool. They represent 16\% of all driver maintainers
and constitute a lower bound estimative of the portion of developers who might
benefit from integrating test tools into their workflows.

Most of the tests performed by individual developers probably occur during the
patch development and review cycle since the Linux kernel community adopts an
RTC (Review Then Commit) policy for incorporating changes.

As the second part of the answer to \emph{RQ1}, we now talk about tests
performed by machines. There is a number of test robots and CI systems testing
the Linux kernel. Throughout this study, we identified a few of them: 0-day,
LKFT, KernelCI, Syzbot, Hulk, CKI, and Buildbot. These rings often provide tests
as a service and incorporate several test tools such as Smatch, Coccinelle, LTP,
Kselftest, libhugetlbfs, v4l2-compliance tests, and many others. Nevertheless,
the set of monitored trees and the frequency each one undergoes testing may vary
from robot to robot. For instance, the 0-day test robot fetches patches from
mailing lists and key developers' trees and has a response time of one hour
around the clock (hence the 0-day name). KernelCI focuses on testing upstream
kernels by generating several kernel build configuration sets and submitting
them to boot tests in various labs worldwide. Syzbot fuzzes main Linux kernel
trees and reports bugs to kernel mailing lists. LKFT focus on testing Linux
release candidates, linux-next, and long-term-stable releases on the arm and
arm64 hardware architectures. LKFT is said to report test results in up to 48
hours.

We recall that 28\% of device driver maintainers reported having registered
their development trees with Linux kernel testing services. This rate rises to
34\% among maintainers with six or more years of Linux development experience.
However, we consider these to be lower-bound estimates of the portion of trees
covered by testing services since most (if not all) development trees are
publicly available and, therefore, can be obtained and tested without consent
from any developer or maintainer. Moreover, because Linux kernel development
trees are accessible, they might be monitored by many other test robots beyond
those identified by our study. 43\% of maintainers who contribute as part of
their jobs said their organizations had a CI system to test the Linux kernel.
Whether the reports from those systems are publicly accessible or not, sharing
test results may contribute to a higher collaboration toward Linux kernel
testing. Indeed, one of the suggestions for improving device driver testing was
for companies to make their test systems publicly accessible.

As a gross estimate of how many developers benefit from test services, we point
out that, respectively, 32\%, 19\%, and 4\% of maintainers said to use the
0-day, KernelCI, and LKFT services with some regularity (often or, at least,
sometimes).

Aside from outlining current Linux kernel testing tools, we indicate
opportunities to improve existing test tools based on suggestions from community
developers (\emph{RQ3}). Starting with Trinity, an ongoing task is
to add support for new system calls and system call flags introduced by more
recent kernel releases. Enhancing support for network protocols and adding tests
for common syscall patterns such as open, read, close was also on the wishlist
of the fuzzer maintainer Dave Jones~\citep{lwn:trinity}. Although Dave suggested
these improvements in 2013, some of those proposals, and many others, remain in
the project's TODO file.

In August 2014, kernel developers desired Kselftest features such as the ability
to execute tests in a few minutes or seconds, run groups of tests at once, and
that test source code was kept in the kernel source tree. A few additional
features were suggested then, but those requests seem to have been fulfilled
throughout the years.

Back in 2016, \citet{lc:kernelci} reported that an area where KernelCI could
improve was in its test coverage. He commented that even though build and boot
regressions were annoying for developers because they impacted everybody working
on the affected configurations and hardware, regressions on peripheral support
or other subsystems not triggered during the boot process could still make
rebases costly.

As more general suggestions regarding Linux kernel testing, we note that in an
interview published in the 2017 Linux Kernel Development
Report~\citep{lf2017:report}, Dan Williams reported that the community should
work on accelerating the growth of a unit test culture for the Linux kernel.
Also, \citet{lj:lk-testing} mentioned that there is no requirement that testers
should be developers.

From our Device Driver Testing Survey results, we point out that 28\% of device
driver maintainers reported having registered their development trees with
automated test services, thus indicating an appreciation of test bots and CI
rings. Nevertheless, from the responses to question 14, we observed various
suggestions for improving and expanding current test services. The directions
are for writing automated tests with mocks, increasing test coverage, increasing
the number of test iterations, and making closed test farms open and publicly
accessible.

Moreover, the lack of hardware to test patches was the top challenge reported by
driver maintainers in responses to question 13. Conversely, solutions that would
allow testing without hardware were also mentioned by many developers in several
questions. For instance, four developers have said using QEMU to test the kernel
in their responses to question 8. Later, QEMU was also cited twice more in
answers to both questions 12 and 14 as a tool that may help driver testing due
to its ability to emulate hardware.

Thus, the evidence we have gathered suggests that the Linux kernel community has
two top desires concerning driver testing. One, to expand existing CI and
automated test systems, and two, to have more emulation and mocking
implementations for testing drivers when the required hardware is unavailable.

\section{Considerations on Linux Kernel Device Driver Tests}

Linux kernel device drivers may be hard to test due to many issues. For example,
one often needs specific hardware to exercise most, if not all, code paths of a
driver. Also, laying devices under particular operating conditions or providing
them with certain stimuli may be difficult. In addition, kernel crashes
complicate the process of getting test output compared to application crashes.
Further, hardware may eventually fail and behave unpredictably.

Alternatively, one could exercise device driver code using emulation,
virtualization, mocking, or software alike. However, it's not clear whether
software can replace hardware for all test purposes. Correctly mimicking
hardware behavior and creating test cases that properly simulate real-world
usage are some challenges to these test approaches.

While CI rings and test robots may compile and boot test the Linux kernel, they
might lack test cases to assess device driver functionality. According to Linux
maintainers who participated in our survey, extending CI systems may be
challenging apart from the hardware access issue because there is ``\textit{no
culture of supplying unit tests and functional tests with drivers.}'' We recall
that \citet{lc:kernelci} has commented about improving the test coverage of
KernelCI and his concerns about regressions in code for driving peripherals.
Moreover, a survey participant reported they only got compile time error reports
from test robots. From such evidence, one might wonder if the current automated
test services provide adequate test coverage for device driver code.

Whatever the answer to this question, we should not disregard automated test
services, for providing high device driver test coverage is a tough affair. One
reason for that is that, even though not all hardware devices need or have an
in-kernel driver for them, the overall number of devices that do need (and have)
in-kernel drivers tends to grow as hardware manufacturers keep releasing new
designs. Moreover, hardware designs may differ a lot from each other, possibly
having particular interfaces, register maps, operating modes, capabilities,
execution contexts, and other distinguishing characteristics that can turn the
process of creating test cases into a device-specific task. Thus, providing
extensive test coverage for Linux kernel device drivers might require a
continuous effort to expand test infrastructure as more drivers are developed.

Thinking in a scenario where hardware devices keep diversifying, we wonder
whether hardware manufacturers shouldn't behave more proactively in leveraging
device driver tests. For example, if manufacturers could provide virtual devices
able to mock their hardware designs, then automated test systems would be able
to use them to exercise additional driver code paths. Thinking further, we
wonder if one could automate the process of creating device mocks from the
hardware design. For instance, if one could generate a state diagram from a
hardware description (such as a VHDL\footnote{VHDL stands for VHSIC Hardware
Description Language. VHSIC is an acronym for Very High Speed Integrated
Circuits.} program), then that model could probably be used to create a QEMU
virtual device to mock the real one. Having accessible device mocks would then
leverage Linux kernel device driver tests.
