%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{get\_driver\_maintainers.awk}
\label{ap:get-maintainers}

In August 2021, Shuah Khan and Kate Stewart launched a survey to Linux kernel
developers. They wanted to assess the effectiveness of Linux Foundation efforts
to encourage the participation of underrepresented groups of developers in the
Linux kernel community. Another goal of their research was to understand the
reasons why developers stop or take a break from active participation in the
community. Thus, they targeted their survey to all developers listed in the
Linux kernel MAINTAINERS file as stated in a snippet of the e-mail they sent to
them.

\begin{verbatim}
``Your input is important to us, hence we are reaching out to you by
sending email to all your contact addresses listed in the MAINTAINERS
file. We apologize if there is duplication.''
\end{verbatim}

Unlike Shuah and Stewart's research, our topic does not relate to all Linux
kernel developers. Instead, we seek especially the opinion and experience of
device driver developers. Thus, we made an AWK program to retrieve only the set
of developers that maintain artifacts under the drivers directory. Designed to
receive a MAINTAINERS file as an argument and print out the names and email
addresses of Linux kernel device driver maintainers, we named it
\textit{get\_driver\_maintainers.awk}. We then contacted device driver
developers by sending our survey to all email addresses obtained from
\textit{get\_driver\_maintainers.awk}. The program source code can be seen
below.

\begin{program}
\centering
\begin{lstlisting}[
	language=Awk,
    style=wider,
    functions={},
    specialidentifiers={},
]
#!/usr/bin/awk -f

BEGIN {
	FPAT = "([^\t<>]+)"
	get_devs = 0 # flag to start recording driver developer's emails
	new = 0 # flag to tell apart each entry
	lost = 0 # sanity check we don't miss any driver maintainer
	print_names = 1
	print_emails = 1
	print_statistics = 1
	named_artifacts = 0 # number of named entries/artifacts under drivers dir
}
{
if (/Maintainers List/)
	get_devs = 1

if (get_devs == 1) {
	if (/^$/) {
		new = 1
		delete emails_buf # clear email buffer
	} else {
		if (/M:\t/)
			emails_buf[$3] = $2 # use email address as key and name as value

		# We expect no M:\t line after a F:\t line within a single entry.
		if (/M:\t/ && new == 0)
			lost++ # Must not happen

		if (/F:\tdrivers\// && new == 1) {
			if (length(emails_buf) > 0) {
				for (email in emails_buf) {
					dev_addresses[email] = emails_buf[email] # hash table
					dev_names[emails_buf[email]] = 0 # to count distinct names
				}
			}
			named_artifacts++;
			new = 0
		}
	}
}
}
END {
	for (email in dev_addresses) {
		if (print_names && print_emails)
			printf "%s <%s>\n", dev_addresses[email], email
		else if (print_names)
			printf "%s\n", dev_addresses[email]
		else if (print_emails)
			printf "%s\n", email
	}
	if (print_statistics) {
		printf "%d named artifacts under drivers dir\n", named_artifacts
		if (lost > 0)
			printf "[Warn] %d uncounted device driver developer(s)!\n", lost
		printf "%d device driver maintainers\n", length(dev_names)
		printf "%d addresses of driver maintainers\n", length(dev_addresses)
	}
}
\end{lstlisting}
\caption{get\_driver\_maintainers.awk.\label{prog:awk}}
\end{program}

We extracted maintainers' email addresses from the Linux kernel 5.17 MAINTAINERS
file because it was the latest stable Linux version by the time we started
inviting developers to answer our survey. Program~\ref{prog:email-list} lists
the set of commands we ran.

\begin{program}
\centering
\begin{lstlisting}[
	language=Bash,
    style=wider,
    functions={},
    specialidentifiers={},
]
wget https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/plain/MAINTAINERS?h=v5.17.8 -O MAINTAINERS_5.17 && \
./get_driver_maintainers.awk MAINTAINERS_5.17 | \
sort | tee email_list
\end{lstlisting}
\caption{Set of commands to extract maintainer's email addresses.\label{prog:email-list}}
\end{program}

%# grep -E "^M:\t*" MAINTAINERS -c
%
%# Obs. about this script's output:
%# - doesn't contain kernel mailing lists (@vger.kernel.org)
%# - contains emails of kernel subsystem maintainers (@kernel.org)
%# - contains emails of people from the LF (@linuxfoundation.org)
%
%# - 1275 endereços de e-mail distintos
%# - 1227 nomes distintos
