#! /bin/python3

# Slightly modified cli.py from https://github.com/damienmarlier51/JupyterNotebookImageExporter
# Obtained under the MIT license

# See https://medium.com/analytics-vidhya/export-images-from-jupyter-notebook-with-a-single-command-422db2b66e92

#from junix.exporter import export_images
from exporter import export_images
import argparse

def main():
    print("iniciar")
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath', type=str, help='Notebook filepath')
    parser.add_argument('-o', '--output_dir', type=str, default=None, help='Directory where to export the images')
    args = parser.parse_args()
    notebook_filepath = args.filepath
    output_directory = args.output_dir
    export_images(notebook_filepath, output_directory)
    print("fim")

main()
